from sqlalchemy import Table, Column, Integer, String, ForeignKey, DateTime, create_engine, select
from sqlalchemy.orm import registry, sessionmaker

from models.birthday import Bday
from models.user import User

mapper_registry = registry()
Session = sessionmaker()

def init_db(db_path: str):
    bday_table = Table(
        "bday",
        mapper_registry.metadata,
        Column("id", Integer, primary_key=True),
        Column("name", String),
        Column("vorname", String),
        Column("datum", DateTime),
        Column("wochentag", String),
        Column("kw", Integer),
        Column("tag", Integer),
        Column("monat", Integer),
        Column("sternzeichen", String),
        Column("raw_text", String),
        Column("inserted_by", Integer, ForeignKey("user.user_id")),
        Column("created", DateTime),
    )

    user_table = Table(
        "user",
        mapper_registry.metadata,
        Column("id", Integer, primary_key=True),
        Column("user_id", Integer),
        Column("name", String),
    )

    mapper_registry.map_imperatively(Bday, bday_table)
    mapper_registry.map_imperatively(User, user_table)

    engine = create_engine(db_path, echo=True)
    mapper_registry.metadata.create_all(engine, checkfirst=True)

    Session.configure(bind=engine, expire_on_commit=False)

def seed_db():
    session = Session()
    sql = select(User).where(User.user_id == 815)
    user = session.execute(sql).first()
    if user is None:
        new_user = User()
        new_user.user_id = 815
        new_user.name = "unknown"
        session.add(new_user)
        session.commit()
