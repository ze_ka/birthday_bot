
from datetime import datetime
from datetime import date
from typing import Optional

from utils.date_helper import get_now_datetime

class Bday():
    """Die Struktur des hinzugefügten Geburtstags"""
    id: Optional[int]
    name: str
    vorname: str
    datum: datetime
    wochentag: str
    kw: int
    tag: int
    monat: int
    sternzeichen: str
    raw_text: str
    inserted_by: int
    created: datetime

    def __init__(self, name, vorname, date, raw) -> None:
        self.name = name
        self.vorname = vorname
        self.datum = date
        self.wochentag = date.isocalendar().weekday
        self.kw = date.isocalendar().week
        self.tag = date.day
        self.monat = date.month
        self.sternzeichen = self._get_signs_of_zodiac(date)['ru']
        self.raw_text = raw
        self.created = get_now_datetime()
        self.inserted_by = 1

    def _get_signs_of_zodiac(self, birthday_datetime: datetime) -> str:
        birth_date = birthday_datetime.date()
        jahr = birthday_datetime.year
        return {
            date(jahr, 12, 22) <= birth_date <= date(jahr, 1, 19): {"ru": "Козерог", "en": "Capricornus (Goat)", "de": "Steinbock"},
            date(jahr, 1, 20) <= birth_date <= date(jahr, 2, 18): {"ru": "Водолей", "en": "Aquarius (Water Bearer)", "de": "Wassermann"},
            date(jahr, 2, 19) <= birth_date <= date(jahr, 3, 20): {"ru": "Рыбы", "en": "Pisces (Fish)", "de": "Fische"},
            date(jahr, 3, 21) <= birth_date <= date(jahr, 4, 19): {"ru": "Овен", "en": "Aries (Ram)", "de": "Widder"},
            date(jahr, 4, 20) <= birth_date <= date(jahr, 5, 20): {"ru": "Телец", "en": "Taurus (Bull)", "de": "Stier"},
            date(jahr, 5, 21) <= birth_date <= date(jahr, 6, 20): {"ru": "Близнецы", "en": "Gemini (Twins)", "de": "Zwillinge"},
            date(jahr, 6, 21) <= birth_date <= date(jahr, 7, 22): {"ru": "Рак", "en": "Cancer (Crab)", "de": "Krebs"},
            date(jahr, 7, 23) <= birth_date <= date(jahr, 8, 22): {"ru": "Лев", "en": "Leo (Lion)", "de": "Löwe"},
            date(jahr, 8, 23) <= birth_date <= date(jahr, 9, 22): {"ru": "Дева", "en": "Virgo (Virgin)", "de": "Jungfrau"},
            date(jahr, 9, 23) <= birth_date <= date(jahr, 10, 22): {"ru": "Весы", "en": "Libra (Balance)", "de": "Waage"},
            date(jahr, 10, 23) <= birth_date <= date(jahr, 11, 21): {"ru": "Скорпион", "en": "Scorpius (Scorpion)", "de": "Skorpion"},
            date(jahr, 11, 22) <= birth_date <= date(jahr, 12, 21): {"ru": "Стрелец", "en": "Sagittarius (Archer)", "de": "Schütze"}
        }[True]
