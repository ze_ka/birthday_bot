"""Custom exceptions thrown by the application"""


class NotCorrectMessage(Exception):
    """Nachricht an den Bot konnte nicht geparst werden"""
    pass
