from typing import Optional

class User:
    id: Optional[int]
    user_id: int
    name: str