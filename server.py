"""Сервер Telegram бота, запускаемый непосредственно"""
import logging
from db.db import init_db, seed_db
import models.exceptions as exceptions
import services.bdaymanager as bday
from utils.middlewares import AccessMiddleware

from dotenv import load_dotenv
import os

from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import CallbackQuery
from aiogram import Bot, Dispatcher, executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import Message
from aiogram.types import Message,KeyboardButton,ReplyKeyboardMarkup,ReplyKeyboardRemove

button_month_stat = KeyboardButton("/month")
button_start = KeyboardButton("/start")
button_expenses = KeyboardButton("/last")
button_categories = KeyboardButton("/categories")

keyboard = ReplyKeyboardMarkup(resize_keyboard=True,one_time_keyboard=True) \
                                        .add(button_month_stat,button_start) \
                                        .add(button_expenses,button_categories)

logging.basicConfig(level=logging.INFO)

if os.getenv('ENV') == 'production':
    load_dotenv('.env.production')
else:
    load_dotenv('.env.dev')
bot = Bot(token=os.getenv("TOKEN_BDAY_BOT"))
dp = Dispatcher(bot)
dp.middleware.setup(AccessMiddleware(os.getenv("TELEGRAM_ACCESS_ID").split(",")))

init_db(os.getenv("DB_PATH"))
seed_db()

@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """Sendet eine Willkommensnachricht"""
    await message.answer(
        "Willkommen bei Geburtstags-Manager \n\n"
        "Gültige Eingabeformate: \n\n"
        "Vorname Name 07.04.1981 \n"
        "Vorname Name 7.4.81 \n"
        "Vorname 7.4 \n"
        "Vorname 7 Mai -> coming soon \n\n"
        "Geburtstage aktueller Monat: /month\n"
        "Zuletzt eingegebene Geburtstage: /last",reply_markup=keyboard)


@dp.message_handler(lambda message: message.text.startswith('/del'))
async def del_row(message: types.Message):
    """Löscht einen Datensatz anhand seiner ID"""
    row_id = int(message.text[4:])
    bday.delete_row(row_id)
    answer_message = f"{row_id} wurde entfernt"
    await message.answer(answer_message)


@dp.message_handler(commands=['month'])
async def month_statistics(message: types.Message):
    """Sendet eine Übersicht für den aktuellen Monat"""
    answer_message = bday.get_currenctmonth_bday()
    await message.answer(answer_message)


@dp.message_handler(commands=['last'])
async def list_bday(message: types.Message):
    """Sendet die letzten gespeicherten Einträge(Geburtstage)"""
    last_bday = bday.last()
    if not last_bday:
        await message.answer("Geburtstage noch nicht festgelegt")
        return

    last_bday_rows = [
        f"{b_day.name} {b_day.vorname} {b_day.datum} "
        f" — drücke "
        f"/del{bday.id} zum löschen "
        for b_day in last_bday]
    answer_message = "Zuletzt gespeicherte Geburtstage:\n\n* " + "\n\n* "\
            .join(last_bday_rows)
    await message.answer(answer_message)


@dp.message_handler()
async def add_bday(message: types.Message):
    """Fügt einen neuen Geburtstag hinzu"""
    try:
        b_day = bday.add(raw_message=message.text, message=message)
    except exceptions.NotCorrectMessage as exception:
        await message.answer(str(exception))
        return
    answer_message = (
        f"{b_day.name} {b_day.vorname} wurde gespeichert")
    await message.answer(answer_message)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
