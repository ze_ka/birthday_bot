""" Работа с др — их добавление, удаление, статистики"""
import datetime
import re
from typing import NamedTuple
#from categories import Categories
from datetime import datetime
from aiogram.types import Message
from models.birthday import Bday
from db.db import Session
import models.exceptions as exceptions
#from categories import Categories
from dateutil import parser

class ParsedMessage(NamedTuple):
    """Die Struktur des hinzugefügten Geburtstags"""
    name: str
    vorname: str
    datum: str

def _parse_birthday(datum) -> datetime:
    """Beschreibung"""
    try:
        return parser.parse(datum, dayfirst=True)
    except ValueError:
        pass
    raise ValueError(
        f"Non-valid date format for field '")


def _parse_user_msg(raw_message: str) -> ParsedMessage:
    """split and set raw message"""
    # - pruefen Format: Alex Silagin 07.04.1981 / group(1) group(2) group(3)
    format1 = r"(^[\w]+) +([\w]+) +([\d]{1,2}.[\d]{1,2}.*[\d]{0,4})"
    format1_match = re.fullmatch(format1, raw_message, re.UNICODE)
    if format1_match:
        return ParsedMessage(vorname=format1_match.group(1),
                             name=format1_match.group(2),
                             datum=format1_match.group(3))

    format2 = r"^([\w]+) +([\d]{1,2}.[\d]{1,2}.*[\d]{0,4})"
    format2_match = re.fullmatch(format2, raw_message, re.UNICODE)
    if format2_match:
        return ParsedMessage(vorname=format2_match.group(1),
                             name='',
                             datum=format2_match.group(2))

    raise exceptions.NotCorrectMessage("Nachricht kann nicht geparst werden. falsches format "
                                       "versuche:\nvorname name | vorname / dd.mm.yyyy ")


def add(raw_message: str, message: Message) -> Bday:
    """Fügt einen neuen Geburtstag hinzu.
    Nimmt als Eingabe den Text der Nachricht, die an den Bot gesendet wurde."""

    # Struktur des hinzugefügten Geburtstags
    user_message = _parse_user_msg(raw_message)
    parsed_birthday = _parse_birthday(user_message.datum)

    parsed_bday = Bday(name=user_message.name, vorname=user_message.vorname,
                       date=parsed_birthday, raw=raw_message)

    session = Session()
    session.add(parsed_bday)
    session.commit()

    return parsed_bday


def get_currenctmonth_bday() -> Bday:
    """Sendet eine Übersicht für den aktuellen Monat"""
    pass


def last() -> Bday:
    """Sendet uletzt gespeicherte Geburtstage"""
    pass
