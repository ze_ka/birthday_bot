import os
import unittest
from dotenv import load_dotenv
from db.db import init_db, seed_db

from services.bdaymanager import _parse_birthday, _parse_user_msg, add

class BdaymanagerTestCases(unittest.TestCase):
    def test_should_parse_multiple_birthday_formats(self):
        bla = _parse_birthday("19.07.1988")
        self.assertEqual(bla.day, 19)

        bla2 = _parse_birthday("07.04.87")
        self.assertEqual(bla2.day, 7)

        bla3 = _parse_birthday("07/04/87")
        self.assertEqual(bla3.day, 7)

    def test_should_parse_user_message_long(self):
        bday = _parse_user_msg("Alexander S 07.04.1988")
        self.assertEqual(bday.name, "S")
        self.assertEqual(bday.vorname, "Alexander")
        self.assertEqual(bday.datum, "07.04.1988")

    def test_should_parse_user_message_short(self):
        bday = _parse_user_msg("Alex 07.04.1988")
        self.assertEqual(bday.vorname, "Alex")
        self.assertEqual(bday.datum, "07.04.1988")
        self.assertEqual(bday.name, '')
    
    """ integration test, uses actual db
    def test_add_integration(self):
        load_dotenv('.env.dev')
        init_db(os.getenv("DB_PATH"))
        seed_db()
        add("Alexander S 19.07.88", None)
    """

if __name__ == '__main__':
    unittest.main()