
from datetime import datetime
from dateutil import tz

def get_now_formatted() -> str:
    """Возвращает сегодняшнюю дату строкой"""
    return get_now_datetime().strftime("%Y-%m-%d %H:%M:%S")

def get_now_datetime() -> datetime:
    """Возвращает сегодняшний datetime с учётом времненной зоны Berlin."""
    zone = tz.gettz("Europe/Berlin")
    now = datetime.now(zone)
    return now