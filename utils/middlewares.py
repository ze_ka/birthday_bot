"""Аутентификация — пропускаем сообщения только от одного Telegram аккаунта"""
from aiogram import types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware
from db.db import Session
from sqlalchemy import select

from models.user import User

class AccessMiddleware(BaseMiddleware):
    def __init__(self, access_id: list):
        self.access_id = access_id
        super().__init__()

    async def on_process_message(self, message: types.Message, _):
        print(f"{message.from_user.first_name} schreinb Nachricht...  ")
        if str(message.from_user.id) not in self.access_id:
            await message.answer("Access Denied / #tschüüüüü")
            raise CancelHandler()
        
        session = Session()
        user_sql = select(User).where(User.user_id == message.from_user.id)
        user = session.execute(user_sql).first()
        if user is None:
            new_user = User()
            new_user.user_id = message.from_user.id
            new_user.name = message.from_user.first_name
            session.add(new_user)
            session.commit()
            print(f"{message.from_user.first_name} wurde hinzugefügt! Willkommen  ")

